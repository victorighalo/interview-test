<?php

namespace App\Http\Controllers;

use App\Events\InvoiceCreated;
use App\Http\Requests\StoreInvoiceRequest;
use App\Http\Resources\InvoiceResource;
use App\Models\Customer;
use App\Models\Invoice;

class InvoicesController extends Controller
{
    public function store(StoreInvoiceRequest $request)
    {
        $customer = Customer::firstOrCreate(
            ['id' => $request->input('customer.id')],
            [
                'email' => $request->input('customer.email'),
                'name' => $request->input('customer.name')
            ]
        );

        $reference = $request->has('invoice.reference') ? $request->input('invoice.reference') : str_pad(Invoice::count() + 1, 5, '00000', STR_PAD_LEFT);

        $invoice = Invoice::firstOrCreate(
            ['reference' => $reference],
            [
                'title' => $request->input('invoice.title'),
                'customer_id' => $customer->id,
                'amount' => $request->input('invoice.amount')
            ]
        );

        InvoiceCreated::dispatch($customer, $invoice);

        return new InvoiceResource($invoice);
    }
}
