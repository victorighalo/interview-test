<?php

namespace App\Listeners;

use App\Events\InvoiceCreated;
use App\Mail\InvoiceGenerated;
use App\Models\Customer;
use App\Models\Invoice;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class SendNewInvoiceWebhook implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  InvoiceCreated  $event
     * @return void
     */
    public function handle(InvoiceCreated $event)
    {
        $timestamp = time();
        $pdfFilePath = "pdfs/{$timestamp}-{$event->_invoice->id}.pdf";

        $pdf = PDF::loadView('pdf_templates.invoices.new', ['invoice' => $event->_invoice]);

        Storage::put($pdfFilePath, $pdf->output());

        Mail::to($event->_customer->email)->queue(new InvoiceGenerated($event->_invoice, $pdfFilePath));

        Http::post('https://webhook.site/312e1b18-a2fd-4670-90a3-785b6b001cbb', [
            'event_type' => 'invoices.new',
            'data' => $event->_invoice->toArray()
        ]);
    }
}
