<?php

namespace App\Listeners;

use App\Events\CustomerCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Http;

class SendNewCustomerWebhook  implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CustomerCreated  $event
     * @return void
     */
    public function handle(CustomerCreated $event)
    {
        Http::post('https://webhook.site/312e1b18-a2fd-4670-90a3-785b6b001cbb', [
            'event_type' => 'customers.new',
            'data' => $event->_customer->toArray()
        ]);
    }
}
