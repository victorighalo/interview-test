<?php

namespace Tests\Unit;

use App\Models\Customer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateInvoiceTest extends TestCase
{
    use RefreshDatabase;

    public function test_it_should_fail_amount_validation()
    {
        $response = $this->postJson('/api/invoices', [
            'customer' => [
            'id' => 44,
            'name' => 'Victor',
            'email' => 'victorighalo@gmail.com',
            ],
            'invoice' => [
                'title' => 'School Fees',
                'amount' => '10A000'
            ]
        ]);
        $response->assertStatus(400);
        $response->assertJsonValidationErrors(['invoice.amount']);
    }

    public function test_it_should_fail_customer_validation()
    {
        $response = $this->postJson('/api/invoices', [
            'invoice' => [
                'title' => 'School Fees',
                'amount' => '109000'
            ]
        ]);
        $response->assertStatus(400);
        $response->assertJsonValidationErrors(['customer.id']);
    }

    public function test_it_should_pass_customer_id_not_required_validation()
    {
        $customer = Customer::factory()->make();
        $this->postJson('/api/invoices', [
            'customer' => [
                'name' => $customer->name,
                'email' => $customer->email,
            ],
            'invoice' => [
                'title' => 'School Fees',
                'amount' => '109000'
            ]
        ])
            ->assertStatus(201)
            ->assertJsonStructure( [
                'data' => [
                        'id',
                        'reference',
                        'title',
                        'customer_id',
                        'amount',
                        'created_at',
                        'updated_at'
                ]
            ] );
    }

    public function test_it_should_pass_customer_name_and_email_not_required_validation()
    {
        $customer = Customer::factory()->create();
        $this->postJson('/api/invoices', [
            'customer' => [
                'id' => $customer->id
            ],
            'invoice' => [
                'title' => 'School Fees',
                'amount' => '109000'
            ]
        ])->assertStatus(201)
            ->assertJsonStructure( [
                'data' => [
                    'id',
                    'reference',
                    'title',
                    'customer_id',
                    'amount',
                    'created_at',
                    'updated_at'
                ]
            ] );
    }
}
